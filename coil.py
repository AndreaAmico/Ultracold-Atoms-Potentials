# -*- coding: utf-8 -*-
# Copyright: 2015 Andrea Amico
# License: BSD

import numpy as np
import matplotlib as matplotlib
import matplotlib.pyplot as plt
import scipy.special as sp
import scipy.ndimage.interpolation as interp

mu0 = 4*np.pi*10**(-7) #H/m
epsilon = 10**(-10)

class Coil(object):
	""" Magnetic field out out of a single loop
	The analytical expression is adopted from: "Simpson, J., Lane, J., Immer, 
	C., & Youngquist, R. (1829). Simple Analytic Expressions for the Magnetic 
	Field of a Circular Current Loop.

	The loop has the centre in (x0,y0,z0) and lies on the x-y plane.
	A small epsilon is added to the equation to avoid a divergence for x=y=0.
	The magnetic field is expressed in Gauss [G]  (10000G = 1T)
	"""
	def __init__(self,x0,y0,z0,I,a,theta=0):
		"""
		(x0,y0,z0) is the centre of the loop.
		I is the current (in Amprere [A]).
		a is the radius of the loop.
		theta is the rotation angle of the loop in the y plane
		"""
		self.x0 = x0
		self.y0 = y0
		self.z0 = z0
		self.I = I
		self.a = a
		self.theta = np.deg2rad(theta)


	def Bx(self,xx,yy,zz):
		"""Return the x component of the magnetic field in the point of space (xx,yy,zz)
		"""
		x = xx-self.x0
		y = yy-self.y0
		z = zz-self.z0
		rho2 = x**2 + y**2
		r2 = x**2 + y**2 + z**2
		alpha2 = self.a**2 + r2 - 2*self.a*np.sqrt(rho2)
		beta2 = self.a**2 + r2 + 2*self.a*np.sqrt(rho2)
		k2 = 1 - alpha2/beta2
		gamma = x**2 - y**2
		C = mu0*self.I/np.pi
		return C*x*z/(2*alpha2*np.sqrt(beta2)*rho2 + epsilon)*\
			((self.a**2+r2)*sp.ellipe(k2)-alpha2*sp.ellipk(k2))*10**4

	def By(self,xx,yy,zz):
		"""Return the y component of the magnetic field in the point of space (xx,yy,zz)
		"""
		x = xx-self.x0
		y = yy-self.y0
		z = zz-self.z0
		rho2 = x**2 + y**2
		r2 = x**2 + y**2 + z**2
		alpha2 = self.a**2 + r2 - 2*self.a*np.sqrt(rho2)
		beta2 = self.a**2 + r2 + 2*self.a*np.sqrt(rho2)
		k2 = 1 - alpha2/beta2
		gamma = x**2 - y**2
		C = mu0*self.I/np.pi
		return C*y*z/(2*alpha2*np.sqrt(beta2)*rho2 + epsilon)*\
			((self.a**2+r2)*sp.ellipe(k2)-alpha2*sp.ellipk(k2))*10**4

	def Bz(self,xx,yy,zz):
		"""Return the z component of the magnetic field in the point of space (xx,yy,zz)
		"""
		x = xx-self.x0
		y = yy-self.y0
		z = zz-self.z0
		rho2 = x**2 + y**2
		r2 = x**2 + y**2 + z**2
		alpha2 = self.a**2 + r2 - 2*self.a*np.sqrt(rho2)
		beta2 = self.a**2 + r2 + 2*self.a*np.sqrt(rho2)
		k2 = 1 - alpha2/beta2
		gamma = x**2 - y**2
		C = mu0*self.I/np.pi
		return C/(2*alpha2*np.sqrt(beta2))*\
			((self.a**2-r2)*sp.ellipe(k2)+alpha2*sp.ellipk(k2))*10**4

	def BxForRot(self,xx,yy,zz):
		x = (xx-self.x0)*np.cos(self.theta) - (zz-self.z0)*np.sin(self.theta)
		y = yy-self.y0
		z = (xx-self.x0)*np.sin(self.theta) + (zz-self.z0)*np.cos(self.theta)
		rho2 = x**2 + y**2
		r2 = x**2 + y**2 + z**2
		alpha2 = self.a**2 + r2 - 2*self.a*np.sqrt(rho2)
		beta2 = self.a**2 + r2 + 2*self.a*np.sqrt(rho2)
		k2 = 1 - alpha2/beta2
		gamma = x**2 - y**2
		C = mu0*self.I/np.pi
		return C*x*z/(2*alpha2*np.sqrt(beta2)*rho2 + epsilon)*\
			((self.a**2+r2)*sp.ellipe(k2)-alpha2*sp.ellipk(k2))*10**4

	def ByForRot(self,xx,yy,zz):
		x = (xx-self.x0)*np.cos(self.theta) - (zz-self.z0)*np.sin(self.theta)
		y = yy-self.y0
		z = (xx-self.x0)*np.sin(self.theta) + (zz-self.z0)*np.cos(self.theta)
		rho2 = x**2 + y**2
		r2 = x**2 + y**2 + z**2
		alpha2 = self.a**2 + r2 - 2*self.a*np.sqrt(rho2)
		beta2 = self.a**2 + r2 + 2*self.a*np.sqrt(rho2)
		k2 = 1 - alpha2/beta2
		gamma = x**2 - y**2
		C = mu0*self.I/np.pi
		return C*y*z/(2*alpha2*np.sqrt(beta2)*rho2 + epsilon)*\
			((self.a**2+r2)*sp.ellipe(k2)-alpha2*sp.ellipk(k2))*10**4

	def BzForRot(self,xx,yy,zz):
		x = (xx-self.x0)*np.cos(self.theta) - (zz-self.z0)*np.sin(self.theta)
		y = yy-self.y0
		z = (xx-self.x0)*np.sin(self.theta) + (zz-self.z0)*np.cos(self.theta)
		rho2 = x**2 + y**2
		r2 = x**2 + y**2 + z**2
		alpha2 = self.a**2 + r2 - 2*self.a*np.sqrt(rho2)
		beta2 = self.a**2 + r2 + 2*self.a*np.sqrt(rho2)
		k2 = 1 - alpha2/beta2
		gamma = x**2 - y**2
		C = mu0*self.I/np.pi
		return C/(2*alpha2*np.sqrt(beta2))*\
			((self.a**2-r2)*sp.ellipe(k2)+alpha2*sp.ellipk(k2))*10**4

	def Bxr(self,xx,yy,zz):
		"""Return the x component of the magnetic field in the point of space (xx,yy,zz)
		including a rotation of an angle theta
		"""
		return self.BxForRot(xx,yy,zz)*np.cos(self.theta) + self.BzForRot(xx,yy,zz)*np.sin(self.theta)

	def Bxr(self,xx,yy,zz):
		"""Return the x component of the magnetic field in the point of space (xx,yy,zz)
		including a rotation of an angle theta
		"""
		return self.ByForRot(xx,yy,zz)

	def Bzr(self,xx,yy,zz):
		"""Return the z component of the magnetic field in the point of space (xx,yy,zz)
		including a rotation of an angle theta
		"""
		return -self.BxForRot(xx,yy,zz)*np.sin(self.theta) + self.BzForRot(xx,yy,zz)*np.cos(self.theta)


# class MultipleCoil(object):
# 	def __init__(self,x0,y0,z0,I,a,r_size,r_loop,z_size,z_loop,theta=0):
# 		self.x0 = x0
# 		self.y0 = y0
# 		self.z0 = z0
# 		self.I = I
# 		self.a = a
# 		self.theta = np.deg2rad(theta)
# 		self.r_size = r_size
# 		self.r_loop = r_loop
# 		self.z_size = z_size
# 		self.z_loop = z_loop


if __name__ == "__main__":

	coil1 = Coil(0,0,0.0,170,0.1)
	coil2 = Coil(0,0,0.1,170,0.1)


	xlim = np.array((-0.001,0.001))
	zlim = np.array((-0.001,0.001))

	fig, ax = plt.subplots()
	zz,xx = np.mgrid[zlim[0]:zlim[1]:30j, xlim[0]:xlim[1]:30j]

	U = coil1.Bx(xx,zz,0)# + coil2.Bz(xx,0,zz)
	V = coil1.By(xx,zz,0)# + coil2.Bx(xx,0,zz)
	W = coil1.Bz(xx,zz,0)# + coil2.Bx(xx,0,zz)

	S = np.sqrt(U**2+V**2+W**2)


	plt.pcolor(zz,xx,S)
	plt.colorbar()
	Q = plt.quiver(zz,xx,U,V,scale=1)

	plt.xlim(zlim*1.1)
	plt.ylim(xlim*1.1)
	plt.show()