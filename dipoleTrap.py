# -*- coding: utf-8 -*-
# Copyright: 2015 Andrea Amico
# License: BSD

import numpy as np
import scipy.constants as const
import matplotlib.pyplot as plt

class DipoleTrap(object):
    def __init__(self,x0,y0,z0,power,waist_y,waist_z,m,lambdaD1,lambdaD2,gamma,lambdaLaser,theta):
        self.c = const.physical_constants["speed of light in vacuum"][0]
        self.h = const.physical_constants["Planck constant"] [0]
        self.hbar = self.h/(2*np.pi)
        self.kB = const.physical_constants["Boltzmann constant"][0]

        self.m = m #kg
        self.lambdaD1 = lambdaD1 #m
        self.lambdaD2 = lambdaD2 #m
        self.gamma = gamma #Hz
        self.omegaD1 = 2*np.pi*self.c/(self.lambdaD1) #Hz
        self.omegaD2 = 2*np.pi*self.c/(self.lambdaD2) #Hz
        self.lambdaLaser = lambdaLaser
        self.omegaLaser = 2*np.pi*self.c/(self.lambdaLaser)
        self.waist_y = waist_y
        self.waist_z = waist_z
        self.power = power
        self.rayleight_y = np.pi*self.waist_y**2/self.lambdaLaser
        self.rayleight_z = np.pi*self.waist_z**2/self.lambdaLaser
        self.x0=x0
        self.y0=y0
        self.z0=z0
        self.theta = np.deg2rad(theta)

    def coupling(self,omega):
        k = -np.pi*self.c**2*self.gamma/2.
        D1 = 1/(self.omegaD1**3)*(1/(self.omegaD1-omega)+1/(self.omegaD1+omega))
        D2 = 1/(self.omegaD2**3)*(1/(self.omegaD2-omega)+1/(self.omegaD2+omega))
        return k*(D1+D2)
    def potential(self,xx,yy,zz):
        xx = xx-self.x0
        yy = yy-self.y0
        zz = zz-self.z0
        k = self.coupling(self.omegaLaser)*2*self.power/(np.pi*self.waist_y*self.waist_z*\
            np.sqrt(1+xx**2/self.rayleight_y**2)**np.sqrt(1+xx**2/self.rayleight_z**2))
        exp_y = np.exp(-2*yy**2/(self.waist_y**2*(1+xx**2/self.rayleight_y**2)**2))
        exp_z = np.exp(-2*zz**2/(self.waist_z**2*(1+xx**2/self.rayleight_z**2)**2))
        return k*exp_y*exp_z/self.kB*10**6

    def potentialRot(self,xx,yy,zz):
        xx = xx-self.x0
        yy = yy-self.y0
        zz = zz-self.z0
        x = xx*np.cos(self.theta) - yy*np.sin(self.theta)
        y = xx*np.sin(self.theta) + yy*np.cos(self.theta)
        z = zz
        k = self.coupling(self.omegaLaser)*2*self.power/(np.pi*self.waist_y*self.waist_z*\
            np.sqrt(1+x**2/self.rayleight_y**2)**np.sqrt(1+x**2/self.rayleight_z**2))
        exp_y = np.exp(-2*y**2/(self.waist_y**2*(1+x**2/self.rayleight_y**2)**2))
        exp_z = np.exp(-2*z**2/(self.waist_z**2*(1+x**2/self.rayleight_z**2)**2))
        return k*exp_y*exp_z/self.kB*10**6


if __name__ == "__main__":
    xlim = np.array((-0.0005,0.0005))
    ylim = np.array((-0.0002,0.0002))

    m = 9.9883414 * 10**(-27) #kg
    lambdaD1 = 670.992421*10**-9 #m
    lambdaD2 = 670.977338*10**-9 #m
    gamma = 2*np.pi*5.8724*10**6 #Hz
    xx,yy = np.mgrid[xlim[0]:xlim[1]:100j, ylim[0]:ylim[1]:100j]

    # self,x0,y0,z0,power,waist_y,waist_z,m,lambdaD1,lambdaD2,gamma,lambdaLaser):
    ggg = DipoleTrap(0,0,0,17*10**-3,85*10**-6,45*10**-6,m,lambdaD1,lambdaD2,gamma,1073*10**-9,0)
    hhh = DipoleTrap(0,0,0,9.5*10**-3,42*10**-6,42*10**-6,m,lambdaD1,lambdaD2,gamma,1064*10**-9,12.75)

    POV = ggg.potentialRot(xx,yy,0)
    POV += hhh.potentialRot(xx,yy,0)


    fig, ax = plt.subplots()
    im = ax.pcolor(xx,yy,-POV)
    PCM=ax.get_children()[2]
    plt.colorbar(PCM, ax=ax) 
    ax.set_xlim(xlim*1.1)
    ax.set_ylim(ylim*1.1)
    plt.show()