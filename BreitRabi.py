# -*- coding: utf-8 -*-
# Copyright: 2015 Andrea Amico
# License: BSD

import numpy as np
import sympy as sy
from sympy import S
from sympy.physics.quantum.cg import CG
import scipy.constants as const
import matplotlib.pyplot as plt


class BreitRabi:
	def __init__(self,I=1.,J=1/2.,A=152.1368407,muB=const.physical_constants["Bohr magneton"][0],gj=2.0023010,gi=-0.000447654):
		F = np.arange(np.abs(I-J),I+J+1,1)
		self.B = sy.symbols("B")
		beta = gj*muB
		gamma = -gi*muB/1836.15267

		self.states = []
		self.cor = {}

		for f in F:
			for fz in np.arange(-f,f+1,1):
				self.states.append((J,I,f,fz))
				self.cor[(J,I,f,fz)] = []

		for state in self.states:
			for jz in np.arange(-state[0],state[0]+1,1):
				for iz in np.arange(-state[1],state[1]+1,1):
					if (float(CG(state[0],jz,state[1],iz,state[2],state[3]).doit())!=0):
						self.cor[(state[0],state[1],state[2],state[3])].append(np.array(\
							[state[0],jz,state[1],iz,float(CG(state[0],jz,state[1],iz,state[2],state[3]).doit())]))

		W = [[0 for col in range(len(self.states))] for row in range(len(self.states))]

		i=0
		for stateI in self.states:
			j=0
			for stateJ in self.states:
				for a in self.cor[stateI]:
					for b in self.cor[stateJ]:
						if np.array_equal(a[:-1],b[:-1]):
							W[i][j] += a[4]*b[4]*(beta*self.B*a[1]+gamma*self.B*a[3])
				j+=1
			i+=1

		i=0
		for s in self.states:
			W[i][i]+= (s[2]*(s[2]+1)-s[0]*(s[0]+1)-s[1]*(s[1]+1))*A/2.
			i+=1

		self.mat = sy.Matrix(W)

	def printStates(self):
		for state in self.states:
			print state

	def printDecomposition(self):
		for item in self.cor:
			print item, self.cor[item]

	def energy(self,field=1,state=0):
		WB = np.matrix(self.mat.subs(self.B,field))
		eig = np.linalg.eigvalsh(WB, UPLO="L")
		return eig[state]

	def getEnergy(self,field,state=0):
		eig = []
		for x in field:
			WB = np.matrix(self.mat.subs(self.B,x))
			eig.append(np.linalg.eigvalsh(WB, UPLO="L"))
		return np.array(eig).T[state]

	def plotBR(self,field=(0,100),filt=[],points=20):
		Bx = np.linspace(field[0],field[1],points)
		eigs = []
		for x in Bx:
			WB = np.matrix(self.mat.subs(self.B,x))
			eigs.append(np.linalg.eigvalsh(WB, UPLO="L"))
		eigs = np.array(eigs)
		if filt:
			for eig in eigs.T[filt]:
				plt.plot(Bx,eig)
		else:
			for eig in eigs.T:
				plt.plot(Bx,eig)
		plt.show()

	def getBR(self,field=(0,100),filt=[],points=20):
		Bx = np.linspace(field[0],field[1],points)
		result = []
		eigs = []
		result.append(Bx)
		for x in Bx:
			WB = np.matrix(self.mat.subs(self.B,x))
			eigs.append(np.linalg.eigvalsh(WB, UPLO="L"))
		eigs = np.array(eigs)
		if filt:
			for eig in eigs.T[filt]:
				result.append(eig)
		else:
			for eig in eigs.T:
				result.append(eig)
		return result



### Analytical computation
# print mat
# eig = mat.eigenvals()
# Bx = np.linspace(0,12,10)
# for key in eig.keys():
# 	print key
# 	f = sy.utilities.lambdify(B,key,"numpy")
# 	plt.plot(Bx,f(Bx))
