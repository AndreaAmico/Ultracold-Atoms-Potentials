# -*- coding: utf-8 -*-
# Copyright: 2015 Andrea Amico
# License: BSD


import numpy as np
import matplotlib as matplotlib
import matplotlib.pyplot as plt
import scipy.constants as const
import scipy.special as sp

import coil as coil
import BreitRabi as BR
import dipoleTrap as DT

c = const.physical_constants["speed of light in vacuum"][0]
h = const.physical_constants["Planck constant"] [0]
hbar = h/(2*np.pi)
kB = const.physical_constants["Boltzmann constant"][0]
muB = const.physical_constants["Bohr magneton"][0]

m = 9.9883414 * 10**(-27) #kg
lambdaD1 = 670.992421*10**-9 #m
lambdaD2 = 670.977338*10**-9 #m
gamma = 2*np.pi*5.8724*10**6 #Hz
mephisto = DT.DipoleTrap(0,0,0,9.5*10**-3,42*10**-6,42*10**-6,m,lambdaD1,lambdaD2,gamma,1064*10**-9,12.75)
IPG = DT.DipoleTrap(0,0,0,17*10**-3,85*10**-6,45*10**-6,m,lambdaD1,lambdaD2,gamma,1073*10**-9,0)

br = BR.BreitRabi(I=1.,J=1/2.,A=152.1368407,muB=muB/h*10**-6*10**-4,gj=2.0023010,gi=-0.000447654)
# br.plotBR()

current = 176
wireSize = 0.004375
rLoop = 7
intRadius = 0.079 + wireSize/2.
extRadius = intRadius + wireSize*(rLoop-1)

zLoop = 8
zStart = 0.039 + wireSize/2.
zStop = zStart + wireSize*(zLoop-1)

z1 = np.linspace(zStart,zStop,zLoop)
a1 = np.linspace(intRadius,extRadius,rLoop)
z2 = np.linspace(-zStop,-zStart,zLoop)
a2 = np.linspace(intRadius,extRadius,rLoop)

coil1 = []
for z in z1:
	for a in a1:
		coil1.append(coil.Coil(0,0,z,current,a))

coil2 = []
for z in z2:
	for a in a2:
		coil2.append(coil.Coil(0,0,z,current,a))

# coil1 = coil.Coil(0,0,0,170,0.05,0)
# coil2 = coil.Coil(0,0,0.2,170,0.1)

xlim = np.array((-0.0004,0.0004))
ylim = np.array((-0.0004,0.0004))
zlim = np.array((-0.0004,0.0004))

xx,yy = np.mgrid[xlim[0]:xlim[1]:60j, ylim[0]:ylim[1]:60j]

dipolePotential = mephisto.potentialRot(xx,yy,0)
dipolePotential += IPG.potentialRot(xx,yy,0)
dipolePotential = dipolePotential - dipolePotential.min()


Bx=0
By=0
Bz=0
for coil in coil1:
    Bx += coil.Bx(xx,yy,0)
    By += coil.By(xx,yy,0)
    Bz += coil.Bz(xx,yy,0)
	
for coil in coil2:
    Bx += coil.Bx(xx,yy,0)
    By += coil.By(xx,yy,0)
    Bz += coil.Bz(xx,yy,0)
	
S = np.sqrt(Bz**2+Bx**2+By**2)
# Bz = coil1.Bz(xx,0,zz)
# Bx = coil1.Bx(xx,0,zz)
# S = np.sqrt(Bz**2+Bx**2)
preShape = S.shape
energy = br.getEnergy(S.flatten(),state=0).reshape(preShape)
energy = (energy-energy.min())*10**12*h/kB


fig, ax = plt.subplots()

def onclick(event):
    x, y = event.xdata, event.ydata
    Bx=0
    By=0
    Bz=0
    for coil in coil1:
        Bx += coil.Bx(x,y,0)
        By += coil.By(x,y,0)
        Bz += coil.Bz(x,y,0)
        
    for coil in coil2:
        Bx += coil.Bx(x,y,0)
        By += coil.By(x,y,0)
        Bz += coil.Bz(x,y,0)
	print 'z = %f, x = %f, B = %f'%(x,y,np.sqrt(Bz**2+Bx**2+By**2))


cid = fig.canvas.mpl_connect('button_press_event', onclick)
im = ax.pcolor(xx,yy,energy+dipolePotential)
PCM=ax.get_children()[2] #get the mappable, the 1st and the 2nd are the x and y axes
plt.colorbar(PCM, ax=ax) 
# ax.quiver(xx,yy,Bx,By,scale=1)


ax.set_xlim(xlim*1.1)
ax.set_ylim(ylim*1.1)
plt.show()



# class Formatter(object):
#     def __init__(self, im):
#         self.im = im
#     def __call__(self, x, y):
#     	z = self.im.get_array()[]
#     	return 'x={:.4f}, y={:.4f}, z={:.4f}'.format(x, y, z)
# ax.format_coord = Formatter(im)