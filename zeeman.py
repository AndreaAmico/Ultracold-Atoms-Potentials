import numpy as np
import sympy as sy
from sympy import S
from sympy.physics.quantum.cg import CG
import matplotlib.pyplot as plt

I = 1.
J = 1/2.
F = np.arange(np.abs(I-J),I+J+1,1)

B = sy.symbols("B")
A = 152.1368407 # MHz
muB = 1.4 # MHz/Gauss *h
gj = 2.0023010
gi = -0.0004476540
beta = gj*muB
gamma = -gi*muB/1836.15267


states = []
cor = {}

for f in F:
	for fz in np.arange(-f,f+1,1):
		states.append((J,I,f,fz))
		cor[(J,I,f,fz)] = []

# for state in states:
# 	print state

for state in states:
	for jz in np.arange(-state[0],state[0]+1,1):
		for iz in np.arange(-state[1],state[1]+1,1):
			if (float(CG(state[0],jz,state[1],iz,state[2],state[3]).doit())!=0):
				cor[(state[0],state[1],state[2],state[3])].append(np.array(\
					[state[0],jz,state[1],iz,float(CG(state[0],jz,state[1],iz,state[2],state[3]).doit())]))

# for item in cor:
# 	print item, cor[item]

W = [[0 for col in range(len(states))] for row in range(len(states))]

i=0
for stateI in states:
	j=0
	for stateJ in states:
		for a in cor[stateI]:
			for b in cor[stateJ]:
				if np.array_equal(a[:-1],b[:-1]):
					W[i][j] += a[4]*b[4]*(beta*B*a[1]+gamma*B*a[3])
		j+=1
	i+=1

i=0
for s in states:
	W[i][i]+= (s[2]*(s[2]+1)-s[0]*(s[0]+1)-s[1]*(s[1]+1))*A/2.
	i+=1

mat = sy.Matrix(W)

### Analytical computation
# print mat
# eig = mat.eigenvals()
# Bx = np.linspace(0,12,10)
# for key in eig.keys():
# 	print key
# 	f = sy.utilities.lambdify(B,key,"numpy")
# 	plt.plot(Bx,f(Bx))


##Numerical computation
eigs = []
Bx = np.linspace(0,40,20)
for x in Bx:
	WB = np.matrix(mat.subs(B,x))
	eigs.append(np.linalg.eigvalsh(WB, UPLO="L"))

eigs = np.array(eigs)
# print WB
for eig in eigs.T:
	plt.plot(Bx,eig,color='b')

plt.xlim([0,40])
plt.ylim([-160,-140])
plt.show()